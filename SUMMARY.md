
# Summary
 
This is the summary of my book.
 
* [Nos valeurs](handbook/values/valeurs.md)
* [Notre Definition of Done](handbook/engineering/definitionOfDone.md)
* [Notre Definition of Ready](handbook/engineering/definitionOfReady.md)
* [Les équipes](handbook/people/teams/team.md)
* [Horaires et accès aux batiments](handbook/reglesclient/HorairesAcces.md)
* [Acceuillir un nouvel arrivant](handbook/people/onboarding/nouvel-arrivant.md)
* [Matériel pour le télétravail](handbook/remote/hardware/materiel.md)
* [Workflow](handbook/workflow/workflows.md)
