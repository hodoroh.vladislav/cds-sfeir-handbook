---
layout: handbook-page-toc
title: "Definition of Ready"
description: Comment évaluer si une spécification est prête pour le développement
canonical_path: "/handbook/engineering/"
---

### Definition of Ready

La "**definition of ready**" (DoR) est un ensemble de critères spécifiques qui **doivent être remplis** pour qu'une spécification soit considérée comme prête à être soumise à l'équipe de développement. 

1.	**Les spécifications sont complètes** : Les spécifications doivent être **complètes** et **bien comprises** par toute l'équipe. Elles doivent inclure **toutes les informations nécessaires** pour les développeurs du sujet puisse travailler dessus sans ambiguïté.

2.	**Les spécifications sont testables** : Chaque sous partie de la demande doivent être testables indépendamment. Cela signifie qu'il doit être possible de **définir des critères d'acceptation clairs** pour s'assurer que les développements sont terminés avec succès.

3.	**Les dépendances sont identifiées et gérées** : Toutes les dépendances nécessaires pour travailler sur les spécifications doivent être **identifiées et gérées au cours de l’analyse**. Si des tâches doivent être réalisées avant la réalisation des spécifications, **elles doivent être traitées avant que les spécifications ne soient considérées comme prêtes**.

4.	**Les ressources sont disponibles** : Les ressources nécessaires pour travailler sur les spécifications doivent être disponibles. Cela peut inclure des **compétences spécifiques** ou des **outils et technologies particuliers** (accès aux outils, profils EID adéquats…).

5.	**Les critères d'acceptation sont définis** : Les **critères d'acceptation doivent être définis en collaboration avec le client**. Ces critères spécifient ce qui doit être satisfait pour que les développements soient considérés comme terminées.

6.	**La priorité est définie** : Les spécifications doit avoir **une priorité claire** pour que l'équipe sache dans quel ordre elle doit travailler dessus.

En respectant tous ces critères, vous pouvez vous assurer que les spécifications sont prêtes à être prise en compte par un développeur. La "definition of ready" est importante pour garantir que l'équipe dispose de toutes les informations et ressources nécessaires pour travailler efficacement sur les spécifications et pour éviter tout retard ou blocage dans le projet.
