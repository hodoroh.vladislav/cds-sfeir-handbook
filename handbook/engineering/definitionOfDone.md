---
layout: handbook-page-toc
title: "Definition of Done"
description: La définition du travail terminé pour les développeurs
canonical_path: "/handbook/engineering/"
---

### Definition of Done

La "**definition of done**" (DoD) est un ensemble de critères spécifiques **qui doivent être remplis** pour qu'une tâche ou une fonctionnalité soit considérée comme terminée et prête à être livrée. 

La "definition of done" inclut les éléments suivants :
1.	**La fonctionnalité est développée et testée avec succès** : la fonctionnalité doit avoir été **développée et testée** pour garantir qu'elle fonctionne correctement et répond aux exigences spécifiées dans les spécifications fonctionnelles.

    o	**Les tests unitaires sont exécutés et passent avec succès** : des tests unitaires doivent être effectués et documentés pour s'assurer que la fonctionnalité fonctionne correctement et que toutes les erreurs ont été corrigées.

    o	**Les tests d'intégration sont exécutés et passent avec succès** : les tests d'intégration doivent être effectués et documentés pour s'assurer que la fonctionnalité s'intègre correctement avec le reste de l'application. Les test d’intégration comprennent aussi des tests de non régression.

    o	**Si possible, le code est révisé et approuvé** : le code doit être révisé pour s'assurer qu'il est de haute qualité, facile à comprendre et à maintenir, et conforme aux normes de codage de l'équipe.

2.	**La documentation est complète et à jour** : toute la documentation nécessaire doit être fournie, y compris les mises à jour de la documentation existante si nécessaire (FME, DC…).

3.	**La fonctionnalité est déployée sur NI** : la fonctionnalité doit être déployée dans l'environnement de NI pour qu'elle soit accessible aux autres développeurs.

4.	**La fonction est validée par le développeur principal** : la fonctionnalité doit être validé par la personne en charge du suivi du projet pour garantir que la fonctionnalité répond à toutes les exigences spécifiées dans les spécifications et répond aux attentes du client.

En respectant tous ces critères, vous pouvez être sûr que la fonctionnalité est de haute qualité, qu'elle fonctionne correctement et qu'elle répond aux exigences de l'utilisateur. La "definition of done" est essentielle pour assurer la qualité et la cohérence du travail de l'équipe de développement.

