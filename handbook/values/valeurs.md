---
layout: handbook-page-toc
title: "Nos valeurs"
description: Découvrez nos valeurs
canonical_path: "/handbook/values/"
---

### À propos de nos valeurs

Les 4 valeurs centrales des CDS SFEIR EID
[**🤝 Collaboration**](#-collaboration),
[**📈 Résultats**](#-résultats),
[**⏱️ Efficacité**](#-efficacité),
[**🌐 Diversité, Inclusion & Appartenance**](#-diversité-inclusion-appartenance)
   

Nous nous inspirons d'autres entreprises et nous optons toujours pour les [solutions ennuyeuses](https://about.gitlab.com/blog/2020/08/18/boring-solutions-faster-iteration/). Nous ajustons continuellement nos valeurs et nous nous efforçons de les améliorer.
Les valeurs des CDS SFEIR sont un document vivant.
Ces valeurs sont inspirées par les valeurs de GitLab.

## 🤝 Collaboration
{:#collaboration .gitlab-purple}

Pour obtenir des résultats, les membres de l'équipe doivent travailler ensemble efficacement. Chez SFEIR, aider les autres est une priorité, même si cela n'est pas directement lié aux objectifs que vous essayez d'atteindre.
De même, vous pouvez compter sur les autres pour obtenir de l'aide et des conseils - en fait, on s'attend à ce que vous le fassiez.
N'importe qui peut intervenir sur n'importe quel sujet.
La personne qui est responsable du travail décide comment le faire,
mais ils doivent toujours prendre chaque suggestion au sérieux et essayer de répondre et d'expliquer pourquoi elle peut ou non avoir été mise en œuvre.

##### Bienveillance
{:.no_toc}
Nous apprécions prendre soin des autres.
Démontrer que nous nous soucions des gens fournit un cadre efficace pour échanger directement et fournir des feedbacks.
Nous sommes tous pour une évaluation précise, mais nous pensons que cela doit être fait avec bienveillance.
Donnez autant de commentaires positifs que possible et faites-le de manière publique.

##### Partage
{:.no_toc}
Certains aspects de la culture SFEIR, tels que la transparence intentionnelle, ne sont pas intuitifs pour les nouveaux membres de l'équipe.
Soyez prêt à investir dans les gens et à engager un dialogue ouvert.
Par exemple, envisagez de rendre publics les problèmes spécifiques dans la mesure du possible afin que nous puissions tous tirer des leçons de votre expérience. N'ayez pas peur du jugement lorsque vous partagez un sujet publiquement, nous comprenons tous [qu'il est impossible de tout savoir](/handbook/values/#Il-est-impossible-de-tout-savoir).

Tout le monde peut **rappeler** à n'importe qui dans l'entreprise nos valeurs.
S'il y a un désaccord sur les interprétations, la discussion peut être portée à plus de personnes au sein de l'entreprise sans répercussions.

Partagez les problèmes que vous rencontrez, demandez de l'aide, fournissez des informations et **parlez-en**.

##### Les feedback négatifs se font en 1-1
{:.no_toc}
Donnez des commentaires négatifs dans le plus petit cadre possible.
Les conversations à l'écart du bureau ou les appels vidéo individuels sont privilégiés.

Le *feedback* négatif est distinct de la négativité et du désaccord. S'il n'y a pas de feedback direct, efforcez-vous de discuter du désaccord [dans un canal public](/handbook/communication/#use-public-channels), de manière respectueuse et [transparente](/handbook/values/#transparency).

Un feedback négatif peut être donnée dans un cadre de groupe s'il s'adresse à une personne à un poste plus élevée dans la chaîne de management. Cela montre que personne n'est au-dessus des feedbacks.

##### Fournir des feedback au moment opportun
{:.no_toc}
Nous voulons résoudre les problèmes tant qu'ils sont **petits**.
Si vous n'êtes pas satisfait de quoi que ce soit (vos fonctions, votre collègue, votre patron, votre salaire, votre emplacement, votre ordinateur), veuillez exprimer vos préoccupations plutôt que de les garder pour vous. Si vous avez besoin d'aller au-delà de votre responsable, vous pouvez envisager de parler à votre EM ou une personne plus expérimentée.

##### Dire merci
{:.no_toc}
Soyez reconnaissant envers les personnes qui vous ont aidé et faite le savoir publiquement, par exemple lors de retrospective, discussion de groupe ou point opérationnel.

Lorsque vous remerciez publiquement, il est important de reconnaître les éléments suivants :

* Remercier dans un cadre aussi large que possible (à l'échelle du CDS) dans une entreprise aussi grande que la nôtre est l'exception au lieu de la norme, il faut un certain temps pour s'y habituer.
* Être remercié en publique pour ce que vous considérez comme une contribution relativement petite ou minuscule peut sembler gênant.
* Remercier une personne doit être fait sincèrement et résumer pourquoi vous êtes reconnaissant afin que la personne qui reçoit puisse facilement comprendre pourquoi elle est remerciée. Même en [assumant une intention positive] (#assume-positive-intent), tout le monde n'est pas à l'aise avec les éloges du public. Aidez cette personne à comprendre comment elle est allée au-delà de ce qu'on attendait d'elle et pourquoi vous pensiez qu'il était important que le membre de l'équipe soit reconnu.
* Il existe un certain nombre de bonnes façons et d'endroits pour dire merci. Nous ne devrions pas limiter les remerciements à skype.

##### Donner un feedback efficace
{:.no_toc}
Donner du feedback est difficile, mais il est important de le livrer efficacement.
Lorsque vous fournissez des commentaires, faites-en toujours référence au travail lui-même ;
concentrez-vous sur l'impact sur le travail et non sur la personne.
Assurez-vous de fournir au moins un exemple clair et récent.
Si une personne traverse une période difficile dans sa vie personnelle, tenez-en compte.
Pour les managers, il est important de réaliser que les membres de l'équipe réagissent à un incident négatif avec leurs managers [six fois plus fortement](https://hbr.org/2013/03/the-delicate-art-of-giving-fee) que ils font à un positif.
En gardant cela à l'esprit, si les conséquences d'une erreur sont moindres que la valeur acquise en fournissant des critiques, il peut être judicieux de garder ce feedback pour vous.
Dans les situations où des commentaires négatifs doivent être donnés, concentrez-vous sur le but de ces commentaires : améliorer les performances du membre de l'équipe à l'avenir.
Faites preuve de reconnaissance généreusement, ouvertement et souvent pour [générer plus d'engagement](http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?infotype=SA&subtype=WH&htmlfid=LOW14298USEN) de la part de votre équipe .

##### Faire connaissance
{:.no_toc}
Au sein de l'équipe, prenez le temps de discuter lors de pauses communes.
Avec le client nous utilisons beaucoup de communication textuelle, et si vous connaissez la personne derrière le texte, il sera plus facile de prévenir les conflits.
Nous encourageons donc les gens à se connaître à un niveau personnel grâce à la communication informelle, par exemple, prenez une pause ou lors de période de télétravail, lors d'appels "pause café" virtuels.

##### Ne jouer par sur le rang
{:.no_toc}
Si vous devez rappeler à quelqu'un le poste que vous occupez dans l'entreprise, vous faites quelque chose de mal.
Expliquez pourquoi vous prenez cette décision et respectez chacun, quelle que soit sa fonction.
Cela inclut l'utilisation du rang d'une autre personne pour vendre une idée ou une décision.

##### Supposer une intention positive
{:.no_toc}
Nous avons naturellement un double standard en ce qui concerne les actions des autres.
Nous blâmons les circonstances pour nos propres erreurs, mais les individus pour les leurs.
Ce double standard est appelé [Erreur d'attribution fondamentale](https://en.wikipedia.org/wiki/Fundamental_attribution_error).
Afin d'atténuer ce biais, vous devez toujours [assumer une intention positive](https://www.collaborativeway.com/general/a-ceos-advice-assume-positive-intent/) dans vos interactions avec les autres, en respectant leur expertise et leur donner le bénéfice du doute face à ce que vous pourriez percevoir comme des erreurs.

Lorsqu'en désaccord, les gens argumentent parfois contre les points les plus faibles d'un argument, ou un argument imaginaire (par exemple ["homme de paille"](https://en.wikipedia.org/wiki/Straw_man)). Supposez que les points sont présentés de bonne foi et essayez plutôt d'argumenter contre la version la plus forte de la position de votre adversaire. Ceci est appelé argumenter contre une position "d'acier", au lieu d'une position "de paille". Ce concept est emprunté à [argue the "steel man"](https://desert.glass/newsletter/week-46/) tel que décrit dans [Robin Sloan](https://www.robinsloan.com/about/) le bulletin d'information.

Une position "d'acier" devrait être contre la version la plus efficace de la position de votre adversaire - potentiellement encore plus convaincante que celle qu'il a présentée. Une bonne position "d'acier" est celle où l'autre personne estime que vous avez bien représenté sa position, même si elle n'est toujours pas d'accord avec vos hypothèses ou votre conclusion.

##### Cibler le comportement, mais n'étiquetez pas les gens
{:.no_toc}
Il y a beaucoup de bien dans [cet article](http://bobsutton.typepad.com/my_weblog/2006/10/the_no_asshole_.html) à propos du fait de ne pas vouloir de toxicité dans nos équipes, mais nous pensons que **toxicité** est une étiquette pour le comportement plutôt qu'une classification inhérente d'une personne. Nous évitons les classifications.

##### Dire pardon
{:.no_toc}
Si vous avez fait une erreur, excusez-vous dès que possible.
S'excuser n'est pas un signe de faiblesse mais un signe de force.
Les personnes qui travaillent le plus feront probablement le plus d'erreurs.
De plus, lorsque nous partageons nos erreurs et y attirons l'attention, les autres peuvent apprendre de nous, et la même erreur est moins susceptible d'être répétée par quelqu'un d'autre.
Les erreurs peuvent inclure le fait que vous n'avez pas été bienveillant avec quelqu'un. Afin de renforcer nos valeurs, il est important, et demande plus de courage, de s'excuser publiquement lorsque vous avez été **méchant** publiquement (par exemple, lorsque vous avez dit quelque chose de méchant ou de non professionnel à un individu ou à un groupe de collegues).

##### Pas d'ego
{:.no_toc}
Ne défendez pas un point pour gagner une dispute ou doublez une erreur.
Vous n'êtes pas votre travail; vous n'avez pas à défendre votre point de vue.
Vous devez rechercher la bonne réponse avec l'aide des autres.

Dans une interview GitLab Unfiltered(https://youtu.be/n9Gfe9p1tmA), Darren M., responsable de GitLab Remote, met en contexte ce principe de fonctionnement.

> Dans de nombreuses organisations, il existe une pression subtile, en fond et persistante pour prouver continuellement votre valeur.
> Et je crois que cela alimente le syndrome de l'imposteur et fait des ravages sur la santé mentale.
>
> Ce qui est si troublant pour moi, c'est la fréquence à laquelle la perception est la réalité.
> En d'autres termes, ceux qui ont maîtrisé l'art d'être perçus comme une élite en retirent des avantages, même si cela n'a rien à voir avec les résultats réels.
>
> Chez GitLab, "pas d'ego" signifie que nous favorisons et soutenons un environnement où les résultats comptent, et vous avez la possibilité d'aborder votre travail de la manière qui vous convient.
> Au lieu de juger les gens qui n'abordent pas le travail d'une manière classique, "pas d'ego" encourage les gens à s'inspirer en regardant les autres aborder le travail de manière nouvelle et différente.

##### Voir les autres réussir
{:.no_toc}
Il est important que nous soyons tous ensemble à célébrer les succès d'autrui. Ceci créé une dynamique positif et un engagement important par l'esprit d'équipe.

##### Ne laissez pas les autres échouer
{:.no_toc}
Gardez un œil sur les autres qui pourraient être en difficulté ou bloqués.
Si vous voyez quelqu'un qui a besoin d'aide, contactez-le et aidez-le, ou mettez-le en contact avec quelqu'un d'autre qui peut fournir une expertise ou une assistance.
Nous réussissons et brillons ensemble !

##### Les gens ne sont pas leur travail
{:.no_toc}
Faites toujours des suggestions sur des exemples de travail, pas sur la personne.
Dites "Vous n'avez pas répondu à mes commentaires sur le document" au lieu de "Vous n'écoutez jamais".
Et, lorsque vous recevez des commentaires, gardez à l'esprit que les commentaires sont le meilleur moyen de vous améliorer et que les autres qui vous donnent des commentaires veulent vous voir réussir.


##### Do it yourself
{:.no_toc}
Notre valeur de collaboration consiste à nous entraider lorsque nous avons des questions, avons besoin de critiques ou avons besoin d'aide.
Inutile de réfléchir, d'attendre un consensus ou de [faire à deux ce que vous pouvez faire vous-même](https://www.inc.com/geoffrey-james/collaboration-is-the-enemy-of-innovation.html). Le manuel Bolt appelle cela la [mentalité du fondateur](https://conscious.org/playbook/founder-mentality/), où tous les membres de l'équipe doivent aborder le problème comme s'ils possédaient l'entreprise.

##### Résolution de problèmes irréprochable
{:.no_toc}
Enquêtez sur les erreurs d'une manière qui se concentre sur les aspects situationnels du mécanisme d'échec et le processus de prise de décision qui a conduit à l'échec, plutôt que de jeter le blâme sur une personne ou une équipe.
Nous devons nous concentrer sur des [analyses des causes profondes](https://codeascraft.com/2012/05/22/blameless-postmortems/) et des rétrospectives sans reproche pour que les parties prenantes s'expriment sans crainte de punition ou de représailles.

##### Soyez acceuillant
{:.no_toc}
Les personnes qui rejoignent l'entreprise disent souvent : « Je ne veux marcher sur les pieds de personne.
Chez SFEIR, nous devrions accepter davantage les personnes qui prennent l'initiative d'essayer d'améliorer les choses.
Au fur et à mesure que les équipes grandissent, leur vitesse de prise de décision diminue car il y a plus de personnes impliquées.
Nous devrions contrecarrer cela en ayant une attitude acceuillante et en nous sentant à l'aise de laisser les autres contribuer à notre domaine.
Par exemple, des commentaires pointus et respectueux sur une proposition d'un référent ou d'un chef de projet peut entrainer un changement. Cependant, il n'est pas tenu de répondre aux commentaires.

##### Il est impossible de tout savoir
{:.no_toc}
Nous savons que nous devons compter sur les autres pour leur expertise que nous n'avons pas.
C'est normal d'admettre que vous ne savez pas quelque chose et de demander de l'aide, même si cela vous rend vulnérable.
Il n'est jamais trop tard pour poser une question, et ce faisant, vous pouvez obtenir les informations dont vous avez besoin pour produire des résultats et renforcer vos propres compétences ainsi que le CDS dans son ensemble.
Après avoir répondu à votre question, veuillez documenter la réponse afin qu'elle puisse être partagée.

Ne vous étonnez pas lorsque les gens disent qu'ils ne savent pas quelque chose, car il est important que tout le monde se sente à l'aise de dire « je ne sais pas » et « je ne comprends pas ».
(Inspiré par [Recurse](https://www.recurse.com/manual#sub-sec-social-rules).)

##### Collaboration : Compétences
{:#collaboration-competency .no_toc}
Les compétences sont le cadre de la source unique de vérité (SSoT) pour les choses que les membres de l'équipe doivent apprendre.
Nous faisons preuve de collaboration lorsque nous prenons des mesures pour aider les autres et incluons les contributions des autres (à la fois internes et externes) (aide et commentaires) pour obtenir le meilleur résultat possible.

<table class="tg">
  <tr>
    <th class="tg-0lax">Rôle dans le projet             </th>
    <th class="tg-0lax">Demontre sa compétence par...</th>
  </tr>
  <tr>
    <td class="tg-0lax">Développeur junior</td>
    <td class="tg-0lax">Le développement de compétences de collaboration en apprenant des autres membres de l'équipe</td>
  </tr>
  <tr>
    <td class="tg-0lax">Développeur</td>
    <td class="tg-0lax">Le développement des compétences de collaboration en utilisant différents types de communication ; priorise sont travail de manière appropriée, s'informe auprès des bonnes personnes (CDS/EID) et utilise les bons moyens de communication.</td>
  </tr>
  <tr>
    <td class="tg-0lax"><br>Développeur expériementé</td>
    <td class="tg-0lax">L'exemplarité de son comportement collaboratif des autres membres de l'équipe projet et sa capacité à le guider dans leurs actions.</td>
  </tr>
  <tr>
    <td class="tg-0lax">Développeur senior</td>
    <td class="tg-0lax">L'encadrement des membres de l'équipe projet sur la manière de travailler plus efficacement et les oriente vers les bonnes solutions techniques et les canaux de collaboration. Il est aussi le garant de la qualité du code de son équipe projet.</td>
  </tr>
  <tr>
    <td class="tg-0lax">Chef de projet</td>
    <td class="tg-0lax">Favorise la prise de décision collaborative et la résolution de problèmes dans le CDS. Assure le suivi des projets, la qualité des livrables et les relations clients en utilisant les outils adéquats. Remonte les informations, risques identifiés et les indicateurs au responsable de CDS.</td>
  </tr>
  <tr>
    <td class="tg-0lax">Chef de projet senior</td>
    <td class="tg-0lax">Favorise la collaboration des équipes de différents projets. Développe des réseaux et construit des partenariats, s'engage dans des activités interfonctionnelles ; collabore avec le client et trouve un terrain d'entente avec un éventail toujours plus large de parties prenantes. Utilise des contacts pour créer et renforcer l'activité</td>
  </tr>
  <tr>
    <td class="tg-0lax">Reponsable de CDS</td>
    <td class="tg-0lax">Dirige la collaboration et le travail d'équipe dans les routines quotidiennes, en donnant la priorité aux interactions, au partage d'informations et à la prise de décision en temps réel entre les projets. Encourage une plus grande collaboration interfonctionnelle entre les chefs de projet.</td>
  </tr>
</table>

## 📈 Résultats
{:#resultats .gitlab-violet}

Nous faisons ce que nous avons promis les uns aux autres et les clients.

##### Mesurez les résultats et non les heures
{:.no_toc}
Nous nous soucions de ce que vous accomplissez : le code que vous avez envoyé, le client que vous avez satisfait et le membre de l'équipe que vous avez aidé. Quelqu'un qui a pris l'après-midi off ne devrait pas avoir l'impression d'avoir fait quelque chose de mal. Vous n'avez pas à défendre la façon dont vous passez votre journée. Nous faisons confiance aux membres de l'équipe pour faire ce qu'il faut au lieu d'avoir des règles rigides. Nous faisons confiance aux membres de l'équipe pour qu'ils se présentent et fassent de leur mieux. N'incitez pas à la concurrence en proclamant combien d'heures vous avez travaillé hier. Si vous travaillez trop d'heures, parlez-en à votre responsable pour discuter de solutions.

##### Donner à l'autonomie
{:.no_toc}
Nous donnons aux gens le pouvoir de se concentrer sur ce qu'ils pensent être le plus bénéfique. Si une réunion ne semble pas intéressante et que la participation active d'une personne n'est pas essentielle au résultat de la réunion, elle peut toujours choisir de ne pas y assister, ou pendant un appel vidéo, elle peut travailler sur d'autres choses si elle le souhaite. Rester dans l'appel peut toujours avoir du sens même si vous travaillez sur d'autres tâches, afin que d'autres pairs puissent vous envoyer un ping et obtenir des réponses rapides en cas de besoin. Ceci est particulièrement utile dans les réunions polyvalentes où vous pouvez être impliqué pendant quelques minutes seulement.

##### Écrivez vos promesses
{:.no_toc}
Mettez-vous d'accord par écrit sur des objectifs mesurables.

##### Ténacité
{:.no_toc}
C'est ce que nous appelons la "persistance du but". Comme mentionné dans [The Influence Blog](https://www.learntoinfluence.com/developing-tenacity-when-facing-opposition/), la ténacité est la capacité de montrer son engagement en ce en quoi vous croyez. Face aux difficultés, il faut se relever, se dépoussiérer, et se remettre rapidement en route après en avoir appris un peu plus.

##### Sentiment d'urgence
{:.no_toc}
Dans un contexte de CDS multi projets, le temps gagné ou perdu a des effets cumulatifs. Essayez d'obtenir les résultats le plus rapidement possible, mais sans compromettre nos autres valeurs et la façons dont nous communiquons, afin la livraison des lots de travaux puisse se faire et que nous puissions nous concentrer sur le prochain projet du back log.

##### Persévérance
{:.no_toc}
Travailler en CDS vous exposera à des situations de différents niveaux de difficulté et de complexité. Cela nécessite de la concentration et la capacité de différer la gratification.
Nous apprécions la capacité à rester concentré et motivé lorsque le travail est difficile et à demander de l'aide en cas de besoin.

##### Pas d'accord, s'engager et être en désaccord
{:.no_toc}
Tout peut être remis en question, mais tant qu'une décision est en place, nous attendons des gens qu'ils s'engagent à l'exécuter. Toutes les décisions et directives passées peuvent être remises en question tant que vous agissez conformément à elles jusqu'à ce qu'elles soient modifiées. C'est [un principe commun](https://ryanestis.com/leadership/disagree-and-commit-to-get-things-done/).
Chaque décision peut être changée;

Dans un contexte de groupe, les participants peuvent être en désaccord avec une proposition mais ne pas exprimer leurs points de vue pour une raison ou une autre. En conséquence, tout le monde perd ses commentaires. La dissenssion est l'expression de ce désaccord. Partagez votre point de vue, plutôt que d'être d'accord simplement pour éviter les conflits ou pour suivre tout le monde.

Lorsque vous souhaitez rouvrir la conversation sur quelque chose, montrez que votre argument est éclairé par des conversations précédentes et [supposez que la décision a été prise avec la meilleure intention](/handbook/values/#Supposer-une-intention-positive).
Vous devez obtenir des résultats pour chaque décision pendant qu'elle est en vigueur, même lorsque vous essayez de la faire changer.
Vous devez communiquer avec la personne qui peut changer la décision au lieu de quelqu'un qui ne le peut pas.

##### Escalader pour débloquer
{:.no_toc}
Nous devons faire preuve de diligence pour définir [les personnes directement responsables](https://medium.com/@mmamet/directly-responsible-individuals-f5009f465da4) (DRI). Les DRI sont habilités à escalader pour débloquer. Une escalade précoce, délivrée avec le contexte du problème, permet aux managers de fonctionner comme un débloqueur.

##### Résultats : Compétences
{:#resultats-competence .no_toc}
Les compétences sont le cadre de la source unique de vérité (SSoT) pour les choses que les membres de l'équipe doivent apprendre.
Nous démontrons des résultats lorsque nous faisons ce que nous nous sommes promis les uns aux autres et aux clients.

| Rôle dans le projet | Démontre une compétence par…	 | Comportement attendu |
|------------------------------------|----------------------------------------|------------------------------------------|
| Développeur junior | Développe les compétences nécessaires pour s'engager et exécuter les actions convenues. | Apprendre/Se développer |
| Développeur | Applique l'engagement aux résultats et démontre sa capacité à exécuter les actions convenues | Grandir/Agir |
| Développeur senior | Modélise un sentiment d'urgence et d'engagement à produire des résultats | Etre un modèle |
| Chef de projet |  Entraîne les membres de l'équipe à collaborer et à travailler de manière efficace vers des résultats en mettant l'accent sur la qualité du résultat | Gérer/Prévoir |
| Chef de projet senior | Favorise une culture d'appropriation de la performance personnelle | Faire grandir |
| Responsable de CDS | Favorise une exécution efficace des résultats en assurant la collaboration entre les membres de l'équipe. Dirige l'atteinte des résultats tout en favorisant l'alignement continu sur nos valeurs de collaboration, d'efficacité, de diversité, d'itération et de transparence. | Conduite de la stratégie et du plan d'action |

## ⏱️ Efficacité
{:#efficacite}

Travailler efficacement sur les bonnes choses nous permet de progresser rapidement, ce qui rend notre travail plus épanouissant.

##### Écrivez les choses
{:.no_toc}
Nous documentons tout : dans le handbook, dans les notes de réunion, dans les FME.
Nous le faisons parce que "[le crayon le plus faible est meilleur que la mémoire la plus nette](https://www.quora.com/What-does-The-faintest-pencil-is-better-than-the-sharpest-memory-mean )."
Il est beaucoup plus efficace de lire un document à votre convenance que d'avoir à demander et à expliquer. Avoir quelque chose dans le contrôle de version permet également à chacun de faire des suggestions pour l'améliorer.

##### Solutions ennuyeuses
{:.no_toc}
Utilisez la solution la plus simple et la plus ennuyeuse pour un problème, et rappelez-vous que [« ennuyeux »](http://boringtechnology.club/) ne doit [pas être confondu avec « mauvais » ou « dette technique ».](http:// mcfunley.com/choose-boring-technology)
La vitesse d'innovation de notre client et de nos évolutions est limitée par la complexité totale qui a été ajoutée jusqu'à présent, donc chaque petite réduction de complexité est utile.
Ne choisissez pas une solution intéressante juste pour rendre votre travail plus amusant ;
l'utilisation d'une solution établie et populaire garantira une expérience plus stable et plus familière pour vous et les autres développeurs.

Faites un effort conscient pour **reconnaître** les contraintes des autres au sein de l'équipe.

##### Libre-service et auto-apprentissage
{:.no_toc}
Les membres de l'équipe doivent d'abord **chercher leurs propres réponses** et, si une réponse n'est pas facilement trouvée ou si la réponse n'est pas claire, demander en public  sans honte. **Notez toute nouvelle information découverte** afin que ceux qui suivront aient une meilleure efficacité en plus de pratiquer la collaboration, l'inclusion et la documentation des résultats.

##### Soyez respectueux du temps des autres
{:.no_toc}
Considérez l'investissement en temps que vous demandez aux autres de faire avec des réunions. Essayez d'éviter les réunions et, si nécessaire, essayez de rendre la participation facultative pour le plus grand nombre de personnes possible. **Toute réunion doit avoir un ordre du jour lié à l'invitation et vous devez documenter le résultat**. 

##### Réponses verbales courtes
{:.no_toc}
Donnez des réponses courtes aux questions verbales afin que l'autre partie ait la possibilité d'en demander plus ou de passer à autre chose.

##### Faites des communications courtes
{:.no_toc}
Gardez les communications écrites de un à plusieurs courtes, comme mentionné dans [cette étude HBR](https://hbr.org/2016/09/bad-writing-is-destroying-your-companys-productivity) : "Une majorité dit que ce qu'ils lisent est souvent inefficace parce que c'est trop long, mal organisé, peu clair, rempli de jargon et imprécis."

##### Etre son propre manager
{:.no_toc}
Nous voulons que chaque membre de l'équipe soit le responsable de ses actions, qui n'ait pas besoin de vérifications quotidiennes pour atteindre ses objectifs. Les membres de l'équipe ont la liberté de gérer leurs actions et leurs initiatives et sont dignes de confiance pour les mener à bien.

##### Liberté et responsabilité plutôt que rigidité
{:.no_toc}
Lorsque cela est possible, nous donnons aux gens la responsabilité de prendre une décision et les en tenons responsables, au lieu d'imposer des règles et des processus d'approbation. Vous devez avoir des objectifs clairs et la liberté d'y travailler comme bon vous semble. La liberté et la responsabilité sont plus efficaces que de suivre un processus de manière rigide, ou de créer des interdépendances, car elles permettent une vitesse de décision plus rapide et des projets mieux gérés.

##### Accepter les erreurs
{:.no_toc}
Tous les problèmes ne doivent pas conduire à un nouveau processus pour les empêcher. Des processus supplémentaires rendent toutes les actions plus inefficaces ; une erreur n'affecte qu'un seul. Une fois que vous avez accepté l'erreur, apprenez-en.

##### Embrasser le changement
{:.no_toc}
L'adoption des fonctionnalités, les besoins des utilisateurs et le fonctionnel changent fréquemment et rapidement. Les entreprises les plus performantes adaptent rapidement leur feuille de route et leur organisation pour suivre le rythme. Les gens peuvent avoir besoin de changer d'équipe, de sujet ou même de responsable. Cela peut à juste titre sembler perturbateur. Si nous nous entraînons à adopter les aspects positifs du changement, tels que des opportunités accrues et de nouvelles choses à apprendre, nous pouvons avancer plus rapidement en tant qu'entreprise et augmenter nos chances de succès. 


##### Efficacité : Compétences
{:#competence-effecacite .no_toc}
Les compétences sont le cadre de la source unique de vérité (SSoT) pour les choses que les membres de l'équipe doivent apprendre.
Nous faisons preuve d'efficacité lorsque nous travaillons sur les bonnes choses, en n'en faisant pas plus que nécessaire et en ne dupliquant pas le travail.
<table class="tg">
  <tr>
    <th class="tg-0lax">Rôle dans le projet</th>
    <th class="tg-0lax">Demonstre sa compétence par…</th>
  </tr>
  <tr>
    <td class="tg-0lax">Développeur junior</td>
    <td class="tg-0lax">Développer une compréhension de l'autogestion : Assumer la responsabilité de vos propres tâches et respecter les
    engagements <br> * Apporte des idées d'amélioration en 1:1. * Apprend à tout écrire car il est beaucoup plus efficace de lire un document à votre convenance que de devoir demander et expliquer.</td>
  </tr>
  <tr>
    <td class="tg-0lax">Développeur</td>
    <td class="tg-0lax">A une compréhension croissante de l'efficacité et agit en faisant ressortir les inefficacités des processus au sein de l'équipe * Cherche des moyens d'être plus efficace dans son rôle, tout en commençant à encadrer les autres sur les moyens de travailler efficacement.</td>
  </tr>
  <tr>
    <td class="tg-0lax"><br>Développeur sénior</td>
    <td class="tg-0lax">Incarne une culture d'efficacité au sein de l'équipe où les gens prennent de bonnes décisions en temps opportun en utilisant les données disponibles et en évaluant plusieurs alternatives * Est un modèle de l'utilisation des solutions ennuyeuses pour augmenter la vitesse d'innovation pour notre équipe et notre client </td>
  </tr>
  <tr>
    <td class="tg-0lax">Chef de projet</td>
    <td class="tg-0lax">*  Prend en charge les inefficacités des processus de sa propre équipe, met en œuvre des efforts inter-équipes pour s'assurer que tout se passe bien. Met en œuvre une méthode de travail dans l'équipe où les membres recherchent d'abord leurs propres réponses et, si une réponse n'est pas facilement trouvée ou si la réponse n'est pas clair, demandez en public car nous devrions tous avoir un faible niveau de honte.
  <tr>
  <tr>
    <td class="tg-0lax">Chef de projet senior</td>
    <td class="tg-0lax">Takes ownership of group level process inefficiencies, guides cross sub-departments in ensuring things are running smoothly <br> Fosters a culture in the sub-departments where you respect others' time and promote self-service and self-learning</td>
  </tr>
  <tr>
    <td class="tg-0lax">Responsable de CDS</td>
    <td class="tg-0lax">Élabore le cadre et la stratégie de l'efficacité. Résultant en des efforts garantissant que les choses fonctionnent bien * Développe les leaders à l'action contre les points d'ineficacité. Tenir les chefs de projet responsables du maintien de cette valeur.</td>
  </tr>
</table>

## 🌐  Diversité, Inclusion & Appartenance
{:#diversite-inclusion .gitlab-purple}

La diversité, l'inclusion et l'appartenance sont fondamentales pour le succès d'un CDS. Nous visons à avoir un impact significatif et faire les efforts pour favoriser un environnement où chacun peut s'épanouir. Nous travaillons pour que chacun se sente le bienvenu. 

##### Comprendre l'impact des micro-agressions
{:.no_toc}

Les micro-agressions sont bien plus que de simples commentaires grossiers ou insensibles. Ils peuvent épuiser les gens en effaçant lentement leur sentiment d'appartenance/de sécurité/d'inclusion au fil du temps. Qu'est-ce qu'une micro-agression ?

> "Les affronts, indignités, dénigrements et insultes quotidiens que les personnes de couleur, les femmes, les populations LGBT ou les personnes marginalisées subissent dans leurs interactions quotidiennes avec les gens." - Derald W. Sue

Dans les CDS SFEIR, nous croyons que chacun a droit à un espace de travail sûr où il peut exprimer qui il est et participer à des conversations sans craindre qu'on lui parle de manière nuisible, étant donné que nous voulons encourager tout le monde à être conscient de ce qu'est une microagression et être conscient de leur impact potentiel.

##### Soyez un mentor
{:.no_toc}
Les gens se sentent plus inclus lorsqu'ils sont soutenus. Pour encourager cela et soutenir un apprentissage diversifié dans tous les CDS, porter vous volontaire pour des projets en dehors de vos technologies ou périmètres fonctionnelles et acceuillez ceux qui désire eux aussi se former lorsque la capacité le permet.

##### Différence
{:.no_toc}
Les choses inattendues et non conventionnelles rendent la vie plus intéressante.
Célébrez et encouragez les dons, les habitudes, les comportements et les points de vue originaux. Un CDS est un excellent moyen d'interagir avec des personnes intéressantes. Nous essayons d'embaucher des personnes qui trouvent dans le travail un excellent moyen de s'exprimer.

##### Construire une communauté sûre
{:.no_toc}
Ne faites **pas** de blagues ou de remarques désobligeantes sur les caractéristiques des personnes qui composent les CDS et comment elles s'identifient.
Tout le monde a le droit de se sentir en sécurité lorsqu'il travaille au sein des CDS SFEIR.
Nous ne tolérons pas les abus, le harcèlement, l'exclusion, la discrimination.
Vous pouvez toujours **refuser** de traiter avec des personnes qui vous traitent mal et vous sortir de situations qui vous mettent mal à l'aise.


##### Préjugés inconscients
{:.no_toc}
Nous reconnaissons que les préjugés inconscients sont quelque chose qui affecte tout le monde et que le
effet qu'il a sur nous en tant qu'êtres humains et notre entreprise est grande.
Nous sommes responsables de comprendre nos propres préjugés implicites et d'aider les autres
comprendre les leurs. Nous travaillons continuellement pour nous améliorer dans ce domaine.

##### Avantages inclus
{:.no_toc}
Nous listons publiquement nos [avantages](https://sites.google.com/a/sfeir.com/sfeirplace/) afin que les gens n'aient pas à demander pendant les entretiens.

##### Réunions inclusives
{:.no_toc}
Soyez consciemment inclusif dans les [meetings](/company/culture/all-remote/meetings/) en donnant à toutes les personnes présentes l'occasion de parler et de présenter leurs points de vue. Cela peut être particulièrement important dans un environnement éloigné.

Pour les réunions internes, envisagez d'utiliser un ordre du jour pour les questions. Par exemple, avec GitLab [Group Conversations](/handbook/group-conversations/), chaque réunion a une liste numérotée à laquelle les membres de l'équipe GitLab peuvent ajouter des questions. Au cours de la réunion, les questions sont répondues à tour de rôle et les discussions sont notées dans le même document. Parfois, ces documents peuvent avoir tellement de trafic (pendant la réunion) que seul un nombre limité de personnes peut modifier le document. Dans ces situations, ceux qui ont des questions doivent publier sur le chat zoom et ceux qui peuvent modifier le document doivent aider à copier la question dans le document. De plus, ceux qui peuvent modifier le document doivent également publier dans le chat zoom pour voir si quelqu'un a des questions qu'ils pourraient aider à ajouter au document afin que les participants à la réunion soient plus en mesure de contribuer à la conversation.

Les clients ne sont pas habitués à travailler de cette façon. Pour favoriser l'inclusion auprès des clients : demandez aux participants leurs objectifs ; assurez-vous pendant les démonstrations que vous faites une pause pour poser une question ; laisser du temps à la discussion.

##### Embrasser la neurodiversité
{:.no_toc}
[La neurodiversité](https://fr.wikipedia.org/wiki/Neurodiversit%C3%A9) fait référence aux variations du cerveau humain concernant l'apprentissage, l'attention, la sociabilité, l'humeur et d'autres fonctions mentales. Il existe diverses conditions neurodéveloppementales, telles que l'autisme, le TDAH, la dyslexie, la dyscalculie, la dyspraxie, les troubles cognitifs, la schizophrénie, la bipolarité et d'autres styles de fonctionnement neurodivergent. Alors que les individus neurodivergents apportent souvent des compétences et capacités uniques qui peuvent être exploitées pour un avantage concurrentiel dans de nombreux domaines (par exemple, la cybersécurité), les individus neurodivergents sont souvent discriminés. En raison de pratiques d'embauche non inclusives, ils ont parfois du mal à passer par les processus d'embauche traditionnels. Les meilleures pratiques d'inclusion de la neurodiversité profitent à tous.

N'oubliez pas que **les cerveaux fonctionnent différemment** et assumez toujours une intention positive, même si quelqu'un se comporte de manière inattendue. Bien qu'il puisse s'agir d'un comportement inattendu pour vous, il peut ne pas l'être pour la personne qui présente ce comportement. C'est la beauté et la valeur de la diversité, embrasser les différences et devenir plus fort et meilleur en conséquence.

La chose la plus importante que les managers puissent faire est de créer un environnement dans lequel tous les membres de l'équipe se sentent suffisamment psychologiquement en sécurité pour exprimer ce dont ils ont besoin pour faire leur travail.
