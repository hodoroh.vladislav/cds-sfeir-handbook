## Exercices mainframe
Une suite d'exercice mainframe a été créé dans les PDS de chaque équipe avec un jcl permetant de les exécuter.

| Equipe | PDS PGM | Nom des PGM | PDS JCL | JCL |
|--------------|--------------|--------------|--------------|------------|
| A740 | --- | --- | --- | --- |
| K940 | --- | --- | --- | --- |
| 8930 | ED.D8930.SOURCE | AELBTSTB / AELBTSTC | ED.D8930.JCL | AELBTST3 |

### DETAILS DES EXERCICES :                                           
#### Notions basiques de COBOL                                    
EXO0  : Démonstration MOVE, DISPLAY, formats                    
EXO1  : Boucle PERFORM VARYING                                  
EXO2  : LECTURE FICHIER READ, ALIM PAR MOVE, AFFICHAGE PAR DISPLAY, ECRITURE FICHIER WRITE                         
EXO3  : LECTURES FICHIERS + APPAREILLAGE 2 FICHIERS, ECRITURE SORTIE                                         
EXO4  : Gestion MOVE numériques/alphanumérique                  
#### COBOL en contexte EID                                        
EXO5  : APPEL DE LA FONCTION L0197 DE AELGAPEL EN UTILISANT : YATMXAR, YAELEED, YAELSED         
EXO6  : Lecture directe base DB2 TABPBUI                     
EXO7  : Lecture de liste TAIVNIR                             
EXO8  : RECEPTION D'UNE DATE EN SYSIN ET VERIFICATION        
EXO9  : Différentes fonctions de ZDAT2000                    
EXO10 : APPEL DE ZPARAPEL POUR LIRE LA TABLE GESPAR A0001143 
EXO11 : Appel service CRA-GetIcnDtl                          
#### Les exercices suivants concernent des cas plus rares      
EXO12 : APPEL DE ATCTSIRO POUR RECHERCHER LE SITE ACTUEL PUIS S'ENVOYER UN MAIL AVEC MESGMAIL                 
EXO13 : APPEL DE SCTZLIST POUR AFFICHER UNE CLAUSE COPY      
EXO14 : TP Appareillage de 3 fichiers                                              