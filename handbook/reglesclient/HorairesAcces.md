---
layout : markdown_page
title: "Horaires et accès"
description: "Règles d'accès aux batiments"
canonical_path: "/ReglesClient/"
---

# Horaires et accès aux batiments
{:.no_toc}

Les CDS SFEIR pour EuroInformation Développement sont hébégés au sein des batiments EID.
De ce fait, les règles d'accèes et les procédures clients sont à réspecter.

## Plages Horaires

Les plages horaires de travail au sein du CDS sont identiques à celles du client :
Une partie fixe (obligation de présence) et une partie variable (présence à la responsabilité du collaborateur)

![Plage horaires client](handbook/images/plages_horaires.png)

## Badge

Le badge "Carte BIP+" est indispensable pour accéder aux bâtiments ainsi qu'à la connexion au pc du client.
Il doit être emporté avec soit dès que l'ont quitte son poste et ne doit être prêté.
En cas d'oublie, un appel au STU-COMMUN pourra autoriser l'accès à son poste de travail en mode "sans carte BIP+" pour 1 journée, cependant les accès aux
bâtiments ne pourront être possible qu'accompagné d'une personne avec son propre badge.

## Parking

Le parking extérieur est accessible par la rue livio, certaines places sont marqués EID, ce sont ces places qui peuvent être utiliser pour se garer.
Le parking sous-terrain contient principalement des places assignées, il peut être utilisé mais sans garanti d'avoir une place libre.

## Presta

L'outil de check in PRESTA est un outil interne du client qui permet d'indiquer sa présence sur site pour des raisons de sécurité.
Ce check-in n'est à réaliser que lorsqu'on est présent sute un site EID.

### En arrivant

En arrivant sur le site, il faut faire son check-in sur PRESTA.
Pour ceci, une fois loggé au compte EID, il faut lancer Edge et écrire "PRESTA" dans la zone express de l'interface EID.
On arrive alors sur la page suivante :

![Splash screen presta](handbook/images/presta1.PNG)

Cet écran récapitule les informations de la personne cherchant à faire son check-in ainsi que le bâtiment concerné.
En cliquant sur suivant, un calendrier de jours de présence sur site est affiché.

![Calendrier presta](handbook/images/presta2.PNG)

Sur cet écran, l'ensemble du mois en cours ainsi que les jours de présence sur site sont affichés, il est possible d'éditer ce calendrier en cliquant sur le bouton modifier en haut à gauche.
Sinon, il suffit de cliquer sur confirmer pour effectuer son check-in.

![Modif presta](handbook/images/presta3modif.PNG)

Les cases cochées indiques les jours de présence sur site.
Pour indiquer un jour de télétravail, il suffit de décocher la période sur cet écran puis de confirmer.

Une fois le check-in réalisé l'écran suivant sera affiché
![Check in](handbook/images/presta4.PNG)

### Avant de partir

Avant de quitter le site, il faudra effectuer un check-out sur PRESTA.
Pour ceci il faut retourner sur l'application PRESTA à partir de la zone express dans l'interface EID.
Il suffit alors de cliquer sur "Je pars du site" et le check-out est réalisé.
