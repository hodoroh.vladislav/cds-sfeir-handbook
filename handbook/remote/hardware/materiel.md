### Bureau

https://www.ikea.com/fr/fr/p/rodulf-bureau-assis-debout-gris-blanc-s99326170/ 

https://www.ikea.com/fr/fr/p/bekant-bureau-assis-debout-plaque-chene-blanchi-blanc-s59282248/ 

### Chaise

https://www.fnac.com/Chaise-de-travail-ergonomique-Urban-Factory-Noir/a15786604/w-4

### Ecrans

https://www.lcd-compare.com/moniteur-LG24BP450YB-LG-24BP450Y-B.htm 

https://www.lcd-compare.com/moniteur-SAMLS24R652FDU-SAMSUNG-S24R652FDU.htm 

https://www.lcd-compare.com/moniteur-VIEVG2440-VIEWSONIC-VG2440.htm 

### Audio

Micro casque

https://www.fnac.com/Casque-Jabra-Evolve-40-Noir/a14641936/w-4#omnsearchpos=6 

https://www.fnac.com/Jabra-Evolve-65-UC-stereo-Micro-casque-sur-oreille-Bluetooth-sans-fil-NFC/a14641935/w-4 

Casque audio

https://www.amazon.fr/dp/B07Q9MJKBV?ascsubtag=AwEAAAAAAAAAAYZ8&linkCode=gs2&tag=thewire03-21&th=1

Micro

https://www.thomann.de/lu/the_t.bone_sc_430_usb_desktop_set.htm

https://www.thomann.de/lu/elgato_wave_1.htm

https://www.fnac.com/mp41449466/M-Audio-Uber-Mic-micro-USB/w-4

### Hub USB

https://www.fnac.com/mp43656751/Adaptateur-MacBook-USB-Type-C-6-en-1-Gris/w-4?oref=1b8d959a-f5e1-a1b6-2dd1-efab876459b0#omnsearchpos=3 

https://www.fnac.com/Hub-It-Works-4-Ports-USB-2-0-Noir/a13104828/w-4#omnsearchpos=2 

### Eclairage

L'espace de travail doit être bien éclairé par le dessus ou une lampe de bureau.

https://jeux-video.fnac.com/Lampe-Elgato-Key-Light-Noir/a14125390/w-4?esl-k=sem-google%7cnu%7cc587634035408%7cm%7ck374702248706%7cp%7ct%7cdc%7ca133727568879%7cg16562157481&gclid=CjwKCAjwm8WZBhBUEiwA178UnK9ororDBnsq7C6ydUPXRNdGWP-EaL0pTqjCGRgs4XNOWhetYmg9NhoC9cIQAvD_BwE&gclsrc=aw.ds&Origin=SEA_GOOGLE_PLA_MICRO

https://www.fnac.com/Panneau-LED-Elgato-Key-Light-Noir/a13540143/w-4

https://www.ldlc.com/fiche/PB00345655.html
