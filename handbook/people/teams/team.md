---
layout: markdown_page
title: "Teams"
description: "Présentation des équipes"
canonical_path: "/people/teams/"
---

# Nos CDS se répartissent sur 3 secteurs

## Assurance : A740


| Nom | Rôle dans les CDS	 | Périmètre | Langages |
|------------------------------------|----------------------------------------|------------------------------------------|------------------------------------------|
| [Michel BRISAC](https://sfeir.workplace.com/profile.php?id=100023093479722) | Gouvernance CDS |  | |
| [Sullivan BEAUGER](https://sfeir.workplace.com/profile.php?id=100085852563968) | Gouvernance CDS |   | |
| [Deepa SINIVASSANE](https://sfeir.workplace.com/profile.php?id=100023999453086) | Analyste Développeur |  | |
| [Bruno CREPEL](https://sfeir.workplace.com/profile.php?id=100026454214174) | Analyste Développeur Senior |  | |
| [Jean-Michel TESSIER](https://sfeir.workplace.com/profile.php?id=100024912933288) | Analyste Développeur Senior|  | |
| [Quentin ANTONY](https://sfeir.workplace.com/profile.php?id=100044483353706) | Développeur |  | |
| [Jérôme NUSS](https://sfeir.workplace.com/profile.php?id=100023644835036) | Développeur |  | |
| [Elvire SCHEILBLING](https://sfeir.workplace.com/profile.php?id=100030757071748) | Développeur |  | |
| [Nicolas KEITH](https://sfeir.workplace.com/profile.php?id=100023994343670) | Développeur Senior/Référent |  | Mainframe |
| [Joachim RAIMUNDO]() | Développeur |  | DevBooster |

## Banque et réseau : K940

| Nom | Rôle dans les CDS	 | Périmètre | Langages |
|------------------------------------|----------------------------------------|------------------------------------------|------------------------------------------|
| [Philippe DOS SANTOS](https://sfeir.workplace.com/profile.php?id=100028731021702) | Gouvernance CDS |  | Mainframe/DevBooster |
| [Anthony NICOLEAU DAVY](https://sfeir.workplace.com/profile.php?id=100035678051844) | Référent |  | Mainframe/DevBooster |
| [Ludovic PAUTLER](https://sfeir.workplace.com/profile.php?id=100023843210374) |Référent |  | Mainframe/DevBooster |
| [Jean ZIELINSKI](https://sfeir.workplace.com/profile.php?id=100041489152877) | Analyste Développeur |  | Mainframe |
| [Paul ROBIN](https://sfeir.workplace.com/profile.php?id=100079912354945) | Analyste Développeur |  | DevBooster |
| [Mohamed CHAOUACHI](https://sfeir.workplace.com/profile.php?id=100079892114570) | Analyste Développeur |  | Mainframe/DevBooster |
| [Rabie BOUGEDRAWI](https://sfeir.workplace.com/profile.php?id=61553096334111) | Analyste Développeur |  | Mainframe |

## DLF : 8930

| Nom | Rôle dans les CDS	 | Périmètre | Langages |
|------------------------------------|----------------------------------------|------------------------------------------|------------------------------------------|
| [Madjid BOUCHAIR](https://sfeir.workplace.com/profile.php?id=100081248112273) | Gouvernance CDS |  | |
| [Anne-Marie HOUZEL](https://sfeir.workplace.com/profile.php?id=100024624635433) | Développeur Senior/Référente | GPRJ GMAT | Mainframe |
| [Vladislav HODOROH](https://sfeir.workplace.com/profile.php?id=100081524364504) |  Développeur Junior | GPRJ GTRA | DevBooster |
| [Gevorg MESROPIAN](https://sfeir.workplace.com/profile.php?id=100085586116281) | Développeur Junior | GPRJ | Mainframe |
| [Ayoub MOUTAIB](https://sfeir.workplace.com/profile.php?id=100080124993283) | Développeur Junior | GPRJ GSAC | DevBooster |
| [Lamine MBAYE](https://sfeir.workplace.com/profile.php?id=61557019617037) | Développeur Senior |  | Mainframe |
