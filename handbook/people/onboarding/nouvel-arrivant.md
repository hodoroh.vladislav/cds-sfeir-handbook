---
layout: markdown_page
title: "Nouvel Arrivant"
description: "Comment acceuillir une nouvelle ressource."
canonical_path: "/people/onboarding/"
---

# Arrivée d'une nouvelle ressource
[[_TOC_]]

## Avant l'arrivée

Avant l'arrivée de la ressource, un certain nombre d'actions doivent être menée par les responsables de CDS

### Accès EID et matériels
- Réception du code RACF (ou SAS ou PersoID), l'identifiant windows (XXXXXXYY) et son mot de passe pour la connexion sans BIP 

<table class="tg">
  <tr>
    <th class="tg-0lax">Centre de service            </th>
    <th class="tg-0lax">Personne à contacter</th>
  </tr>
  <tr>
    <td class="tg-0lax">Assurance</td>
    <td class="tg-0lax">D. Keyling : delphine.keyling@e-i.com</td>
  </tr>
  <tr>
    <td class="tg-0lax">Banque et réseau</td>
    <td class="tg-0lax">S. Grauss : sebastien.grauss@e-i.com</td>
  </tr>
  <tr>
    <td class="tg-0lax">DLF</td>
    <td class="tg-0lax">B. Zimmerlé : benoit.zimmerle@e-i.com</td>
  </tr>
</table>

- Vérification de la présence dans [ZCOPTEL](https://deskportal2003-si.cm-cic.fr/ztel_zcoptel/?mnc=zcoptel&site=SI&banque=10278&caisse=00111&itaret=et%3DConsole%26s%3DStepActivityConsole%253a%253a%26kap%3DBOUCHAMJ%26kas%3D0%26kao%3D0%26kapilrchclamaq%3D1&langue=FR&look=INTRANET_2020&mncret=sara&pays=FR&profil=INFO21) 

<table class="tg">
  <tr>
    <th class="tg-0lax">Centre de service            </th>
    <th class="tg-0lax">Référence</th>
  </tr>
  <tr>
    <td class="tg-0lax">Assurance</td>
    <td class="tg-0lax">A740</td>
  </tr>
  <tr>
    <td class="tg-0lax">Banque et réseau</td>
    <td class="tg-0lax">K940</td>
  </tr>
  <tr>
    <td class="tg-0lax">DLF</td>
    <td class="tg-0lax">8930</td>
  </tr>
</table>

- Vérification de la présence dans [SARA](https://sarah24-si.cm-cic.fr/hdes_saraz/default.aspx?_siteret=SI&acceptcultures=FRFR%20DEDE%20ESES%20ENGB%20FRBE%20NLBE%20PTPT&mnc=sara&banque=10278&caisse=00111&langue=FR&look=INTRANET_2020&mncret=gepr&pays=FR&profil=INFO21) 


![Acceder à SARA puis rechercher une équipe dans le menu de gauche](handbook/images/sara1.PNG)
![Renseigner le numéro d'équipe puis cliquer dans la liste afin d'afficher l'effectif](handbook/images/sara2.PNG) 

<table class="tg">
  <tr>
    <th class="tg-0lax">Centre de service            </th>
    <th class="tg-0lax">Référence</th>
  </tr>
  <tr>
    <td class="tg-0lax">Assurance</td>
    <td class="tg-0lax">A740</td>
  </tr>
  <tr>
    <td class="tg-0lax">Banque et réseau</td>
    <td class="tg-0lax">K940</td>
  </tr>
  <tr>
    <td class="tg-0lax">DLF</td>
    <td class="tg-0lax">8930</td>
  </tr>
</table>

- Vérification de l'ajout à la liste de diffusion du périmètre

<table class="tg">
  <tr>
    <th class="tg-0lax">Centre de service            </th>
    <th class="tg-0lax">Liste de diffusion</th>
  </tr>
  <tr>
    <td class="tg-0lax">Assurance</td>
    <td class="tg-0lax">LD EIDP DVLP A740</td>
  </tr>
  <tr>
    <td class="tg-0lax">Banque et réseau</td>
    <td class="tg-0lax">LD K940</td>
  </tr>
  <tr>
    <td class="tg-0lax">DLF</td>
    <td class="tg-0lax">LD EID DLF 8930 EXTERNE</td>
  </tr>
</table>

- Préparation du poste de travail
    -   Deux écrans
    -   Un clavier
    -   Une sourie
    -   Un micro casque
    -   Une tour EURO Informations (**rekit si poste existant**)

-   Fiche EMMA à communiquer à EURO Informations
    -   Transmission des numéros PFXX par email

<table class="tg">
  <tr>
    <th class="tg-0lax">Centre de service            </th>
    <th class="tg-0lax">Personne à contacter</th>
  </tr>
  <tr>
    <td class="tg-0lax">Assurance</td>
    <td class="tg-0lax">D. Keyling</td>
  </tr>
  <tr>
    <td class="tg-0lax">Banque et réseau</td>
    <td class="tg-0lax">S. Grauss</td>
  </tr>
  <tr>
    <td class="tg-0lax">DLF</td>
    <td class="tg-0lax">B. Zimmerlé</td>
  </tr>
</table>

- Mise à jour du fichier de matériel

<table class="tg">
  <tr>
    <th class="tg-0lax">Centre de service            </th>
    <th class="tg-0lax">lien vers le fichier</th>
  </tr>
  <tr>
    <td class="tg-0lax">Assurance</td>
    <td class="tg-0lax">\Documentation\1-Orga&Methodes (SMQ, Orga CDS,...)\3 - Démarches\Extrait matériel Sfeir à tenir à jour.xlsx</td>
  </tr>
  <tr>
    <td class="tg-0lax">Banque et réseau</td>
    <td class="tg-0lax">???</td>
  </tr>
  <tr>
    <td class="tg-0lax">DLF</td>
    <td class="tg-0lax">\Documentation\99 - Matériel</td>
  </tr>
</table>

- Récupération de la carte BIP
La carte BIP est envoyé aux secrétaires par lettre et doit être récupéré si non redirigé vers Latitude

### Droits et communications

### Intégration aux documents de l'équipe

## Après l'arrivée

### Mode sans BIP
La ressource doit appeler le STU-COM : **0388143951** et demander le passage en mode sans BIP.
Le STU-COM lui fournira un mot de passe pour se connecter.
Une fois connecté, il sera demandé de mettre à jour ce code (8 caractères alphanumériques avec au moins 1 majuscule, 1 minuscule et 1 chiffre).

### Mode carte BIP+
Après avoir reçu le code provisoire de la carte BIP+, il faut **générer un certificat** en cliquant sur la boule rouge clignotant dans la barre de lancement.
Une fois ce certificat généré sur la carte BIP+, il est alors possible de personnaliser le code secret (6 chiffres)

